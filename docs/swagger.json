{
    "schemes": [
        "http",
        "https"
    ],
    "swagger": "2.0",
    "info": {
        "description": "Events Delivery Service",
        "title": "events-svc",
        "termsOfService": "https://educationplannerbc.ca/terms/",
        "contact": {
            "name": "API Developer Support",
            "url": "https://educationplannerbc.ca/support",
            "email": "support@educationplannerbc.ca"
        },
        "version": "1.0"
    },
    "basePath": "/events-api/v1",
    "paths": {
        "/events-api/v1/admin/loglevel": {
            "get": {
                "description": "gets the service log level (one of Trace, Debug, Info, Warn or Error)",
                "produces": [
                    "application/json"
                ],
                "summary": "gets the current service log level",
                "operationId": "get-get-loglevel",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/events.Message"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            },
            "put": {
                "description": "dynamically sets the service log level to one of Trace, Debug, Info, Warn or Error",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "summary": "sets the service log level",
                "operationId": "get-set-loglevel",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Log Level",
                        "name": "verbosity",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/events.Message"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/events-api/v1/health": {
            "get": {
                "description": "checks health of events service",
                "produces": [
                    "text/plain"
                ],
                "summary": "check health of events service",
                "operationId": "get-health",
                "responses": {
                    "200": {
                        "description": "ok",
                        "schema": {
                            "type": "string"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/events-api/v1/records": {
            "get": {
                "description": "gets a filtered array of JSON journal records",
                "produces": [
                    "application/json"
                ],
                "summary": "gets a filtered array of JSON journal records",
                "operationId": "get-records",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/events.RecordsResponse"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/events-api/v1/stats": {
            "get": {
                "description": "get events service statistics",
                "produces": [
                    "application/json"
                ],
                "summary": "get events service statistics",
                "operationId": "get-stats",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/events.Stats"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/info": {
            "get": {
                "description": "get events service info, including version, log level",
                "produces": [
                    "application/json"
                ],
                "summary": "get events service info",
                "operationId": "get-info",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/build.Runtime"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.ErrorResponse"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "build.BuildInfo": {
            "type": "object",
            "properties": {
                "command": {
                    "type": "string"
                },
                "dependencies": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "dirtyBuild": {
                    "type": "boolean"
                },
                "goVersion": {
                    "type": "string"
                },
                "lastCommit": {
                    "type": "string"
                },
                "project": {
                    "type": "string"
                },
                "revision": {
                    "type": "string"
                },
                "version": {
                    "type": "string"
                }
            }
        },
        "build.Runtime": {
            "type": "object",
            "properties": {
                "buildInfo": {
                    "$ref": "#/definitions/build.BuildInfo"
                },
                "logLevel": {
                    "type": "string"
                },
                "serviceInfo": {
                    "type": "object",
                    "additionalProperties": {
                        "type": "string"
                    }
                }
            }
        },
        "events.Message": {
            "type": "object",
            "properties": {
                "action": {
                    "type": "string"
                },
                "actorID": {
                    "type": "string"
                },
                "actorType": {
                    "type": "string"
                },
                "error": {
                    "type": "string"
                },
                "objectID": {
                    "type": "string"
                },
                "objectType": {
                    "type": "string"
                },
                "targets": {
                    "type": "string"
                },
                "timestamp": {
                    "type": "integer"
                }
            }
        },
        "events.Record": {
            "type": "object",
            "properties": {
                "action": {
                    "type": "string"
                },
                "actorID": {
                    "type": "string"
                },
                "actorType": {
                    "type": "string"
                },
                "error": {
                    "type": "string"
                },
                "objectID": {
                    "type": "string"
                },
                "objectType": {
                    "type": "string"
                },
                "timestamp": {
                    "type": "integer"
                }
            }
        },
        "events.RecordsResponse": {
            "type": "object",
            "properties": {
                "count": {
                    "type": "integer"
                },
                "data": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/events.Record"
                    }
                },
                "next": {
                    "type": "integer"
                },
                "offset": {
                    "type": "integer"
                },
                "total": {
                    "type": "integer"
                }
            }
        },
        "events.Stats": {
            "type": "object",
            "properties": {
                "idle": {
                    "type": "integer"
                },
                "inUse": {
                    "type": "integer"
                },
                "maxIdleClosed": {
                    "type": "integer"
                },
                "maxLifetimeClosed": {
                    "type": "integer"
                },
                "maxOpenConnections": {
                    "type": "integer"
                },
                "openConnections": {
                    "type": "integer"
                },
                "waitCount": {
                    "type": "integer"
                },
                "waitDuration": {
                    "type": "integer"
                }
            }
        },
        "http.ErrorResponse": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                },
                "timestamp": {
                    "type": "integer"
                }
            }
        }
    }
}