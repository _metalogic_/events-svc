package nats

import (
	"time"

	"bitbucket.org/_metalogic_/config"
	events "bitbucket.org/_metalogic_/events-svc"
	"bitbucket.org/_metalogic_/log"
	"github.com/nats-io/nats.go"
)

var (
	subject = "events.*"
	queue   = "events-svc"
)

// listener implements the Events Listener interface
type listener struct {
	cluster  string
	clientID string
	conn     *nats.Conn
	sub      *nats.Subscription
	opts     []nats.Option
	info     map[string]string
	init     func() error
	quit     chan bool
}

// register a listener as a side-effect of package import
func init() {
	subject = config.IfGetenv("NATS_EVENTS_SUBJECT", subject)
	queue = config.IfGetenv("NATS_EVENTS_QUEUE", queue)
	events.RegisterListener(newNATSListener("NATS Events Listener"))
}

func newNATSListener(name string) (svc *listener) {

	var err error
	svc = &listener{
		cluster: config.IfGetenv("NATS_CLUSTER", ""),
		opts:    setupConnOptions("NATS Events Listener"),
		info:    make(map[string]string),
		init: func() (err error) {
			log.Debug("init NATS listener")
			return nil
		},
		quit: make(chan bool, 1),
	}

	svc.info["Type"] = "NATS"

	// Connect to NATS
	servers := config.MustGetenv("NATS_SERVERS")
	svc.conn, err = nats.Connect(servers, svc.opts...)
	if err != nil {
		log.Fatal(err)
	}
	return svc
}

// Listen receives events messages on a NATS queue, then calling journal.Record(msg) to handle the message;
// Listen only exits on error or interrupt signal
func (svc listener) Listen(journal events.Journal) (err error) {
	svc.sub, err = svc.conn.QueueSubscribe(subject, queue, func(m *nats.Msg) {
		log.Tracef("[%s] Received on [%s] Queue[%s]: '%s'", svc.clientID, m.Subject, m.Sub.Queue, string(m.Data))
		msg, err := events.DecodeMessage(m.Subject, m.Data)
		if err != nil {
			log.Error(err)
			return
		}
		journal.Record(msg)
	})
	if err != nil {
		return err
	}

	svc.conn.Flush()

	if err = svc.conn.LastError(); err != nil {
		log.Fatal(err)
	}

	log.Infof("Listening on [%s]", queue)

	// wait for quit on close
	<-svc.quit

	log.Warningf("draining %s listener %s ...", svc.ID(), svc.clientID)
	svc.conn.Drain()
	log.Warningf("unsubscribing...")
	svc.sub.Unsubscribe()
	log.Warningf("exiting %s listener %s", svc.ID(), svc.clientID)

	return nil
}

// Close closes NATS event listener
func (svc listener) Close() {
	svc.quit <- true
}

// Health checks to see if NATS service is healthy
// TODO
// Ref: https://docs.nats.io/nats-server/configuration/monitoring
func (svc listener) Health() error {
	return nil
}

func (svc listener) ID() string {
	return "NATS"
}

// Info returns information about the Service.
func (svc listener) Info() (info map[string]string) {
	return svc.info
}

func (svc listener) Init() error {
	return svc.init()
}

// Stats returns Service  statistics
func (svc listener) Stats() string {
	return "TODO"
}

func setupConnOptions(name string) (opts []nats.Option) {
	totalWait := 10 * time.Minute
	reconnectDelay := time.Second

	opts = []nats.Option{nats.Name(name)}
	opts = append(opts, nats.ReconnectWait(reconnectDelay))
	opts = append(opts, nats.MaxReconnects(int(totalWait/reconnectDelay)))
	opts = append(opts, nats.DisconnectHandler(func(nc *nats.Conn) {
		log.Warningf("Disconnected: will attempt reconnects for %.0fm", totalWait.Minutes())
	}))
	opts = append(opts, nats.ReconnectHandler(func(nc *nats.Conn) {
		log.Warningf("Reconnected [%s]", nc.ConnectedUrl())
	}))
	opts = append(opts, nats.ClosedHandler(func(nc *nats.Conn) {
		log.Warningf("%s was closed", nc.Opts.Name)
	}))
	return opts
}
