# EPBC Event Service

## Build Docker Image

```
$ docker build -t metalogic/events-svc .
```

<br /><br />
## Run nats
To run a nats-streaming server for docker use:<br />
docker run --name test-cluster -p 4222:4222 -ti nats-streaming:latest

### Run locally
To run events-svc locally alongside other APIs, I use the following environment:<br />
"NATS_NEW_CLUSTER": "test-cluster",<br />
"NATS_NEW_SERVERS": "nats://0.0.0.0:4222",<br />
"NATS_ACK_DELAY": "180s",<br />
"NEW_RELIC_APM_NAME": "Metrics Service",<br />
"NEW_RELIC_PASSWORD": {secret},<br />
"METRICS_PUSH_INTERVAL": "20s",<br />

And I change my port as well, to avoid intersection with other APIs:<br />
"port": 8081,

In main.go I also change the port:<br />
log.Fatal(handler.ServeHTTP(":8081"))

<br /><br />
## New Relic

Currently we have New Relic as our only observer tool. Through events-svc we send all our metrics and traces to New Relic, which allows us to view our data in neat diagrams and graphs on the New Relic homepage. 

Any metrics data sent to the events-svc will be in the format: 
```
{ "service":"Service", "message":"Message" } 
```
Where "Service" specifies what type of data is send and "Message" contains a JSON object, in string form, with events or tracing data.

### Tracing
To create a trace through events-svc, there is two "Service"s that should be utilized. The first service is **StartSpan** which accepts the following data: 
```
"{\"api\":\"Api\",\"identifier\":\"Identifier\",\"parentIdentifier\":\"ParentIdentifier\",\"name\":\"Name\",\"start\":\"Start\"}"
```
"Identifier" is a GUID or Tracing ID that is used to keep track of the specific trace, "ParentIdentifier" is a different GUID or Tracing ID used to create subtraces, "Name" is a quick way to identify the origin of a trace and "Start" is when the trace started.

The other "Service" is **EndSpan** and it accepts the following data:
```
"{\"identifier\":\"Identifier\",\"end\":\"End\"}"
```
"End" is when the trace ended.

Below is a picture of a trace with two subtraces on New Relic:
![Tracing](/gitWiki/tracing.png)

### Counting
The main "Service" to store counting data through the events-svc is **CountEndpoint** which accepts the following data:
```
"{\"api\":\"Api\",\"epbcID\":\"EPBCID\",\"endpoint\":\"Endpoint\",\"rollback\":\"Rollback\",\"hasError\":false}"
```
"API" is used to lump data together, "Endpoint" is used to differentiate between what endpoints were called and "Rollback" has information about whether or not a rollback occured.

Below is a picture from New Relic that shows which endpoints have been called the most:
![Tracing](/gitWiki/countEndpoint.png)

<br /><br />
And a picture that shows how many errors occured calling *SetVoucher*:
![Tracing](/gitWiki/countErrors.png)

<br /><br />
## Metrics usage
Tracking metrics in any API can be achieved, if there is a working metrics and  service in the API. Any data tracking using **Count{Stuff}** is pretty straigth forward, it takes some data and logs it to the metrics-svc. The **StartSpan** and **EndSpan** endpoints are a bit more complicated, as a started span should always be closed if possible. 

When actually implementing metrics for a specific endpoint, one of the easiest ways I found was to use anonymous methods. Doing this I avoid having to call **EndSpan** after each return statement, and can instead just call it once after the anonymous function has finished running. An example of this can be seen below:
```
func Endpoint() {
    StartSpan()
    func () {
        //actual endpoint logic
    }()
    EndSpan()
}
```
