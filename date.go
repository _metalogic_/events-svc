package events

import (
	"strings"
	"time"
)

// Date is a custom date
type Date struct {
	time.Time
}

// UnmarshalJSON ...
func (d *Date) UnmarshalJSON(input []byte) error {
	if input == nil {
		return nil
	}
	strInput := string(input)
	strInput = strings.Trim(strInput, `"`)
	newTime, err := time.Parse("2006-01-02", strInput)
	if err != nil {
		return err
	}

	d.Time = newTime
	return nil
}

// DateTime converts Date to a time.Time
func (d *Date) DateTime() time.Time {
	return d.Time
}
