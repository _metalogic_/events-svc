package server

import (
	"fmt"
	"net/http"

	"bitbucket.org/_metalogic_/config"
	events "bitbucket.org/_metalogic_/events-svc"
	. "bitbucket.org/_metalogic_/glib/http" // dot import fo avoid package prefix in reference (shutup lint)
	_ "bitbucket.org/_metalogic_/glib/types"
	"bitbucket.org/_metalogic_/log"
)

// @Summary gets a filtered array of JSON journal records
// @Description gets a filtered array of JSON journal records
// @ID get-records
// @Produce json
// @Success 200 {object} events.RecordsResponse
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /events-api/v1/records [get]
func Records(svc events.Journal) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		var (
			actorType  string
			actorID    string
			action     string
			objectType string
			objectID   string
			failed     *bool
			from       int64
			to         int64
			offset     int
			limit      int
			err        error
		)

		if ValidateQuery(r, "actorType", "actorID", "action", "objectType", "objectID", "failed", "from", "to", "offset", "limit"); err != nil {
			ErrJSON(w, NewBadRequestError(err.Error()))
			return
		}

		actorType = StringParam(r, "actorType", "")
		actorID = StringParam(r, "actorID", "")
		action = StringParam(r, "action", "")
		objectType = StringParam(r, "objectType", "")
		objectID = StringParam(r, "objectID", "")

		failed, err = TernaryParam(r, "failed")
		if err != nil {
			ErrJSON(w, NewBadRequestError(fmt.Sprintf("invalid value for query parameter 'failed': %s", err)))
			return
		}

		from, err = Int64Param(r, "from", 0)
		if err != nil {
			ErrJSON(w, NewBadRequestError(fmt.Sprintf("invalid value for query parameter 'from': %s", err)))
			return
		}
		to, err = Int64Param(r, "to", 0)
		if err != nil {
			ErrJSON(w, NewBadRequestError(fmt.Sprintf("invalid value for query parameter 'to': %s", err)))
			return
		}

		offset, err = IntParam(r, "offset", 0)
		if err != nil {
			ErrJSON(w, NewBadRequestError(fmt.Sprintf("invalid value for query parameter 'offset': %s", err)))
			return
		}
		if !(offset >= 0) {
			ErrJSON(w, NewBadRequestError(fmt.Sprintf("invalid value for query parameter 'offset': %d", offset)))
			return
		}

		/*
			To prevent killing the backend by running a long query we need a limit on the number of returned vouchers.
			The limit can be passed as a query parameter and if it's not passed, then we read the value of config parameter
			"PAGINATION_DEFAULT_LIMIT". If "PAGINATION_DEFAULT_LIMIT" is not set then default is 20.

			We also need a maximum number of results shown in a page (pagMaxLimit). We read this value from config parameter
			"PAGINATION_MAX_LIMIT". If this parameter is not set, then default is 100. If the value of "limit" exceeds
			"pagMaxLimit", then "limit" will be set to "pagMaxLimit".
		*/

		limit, e := IntParam(r, "limit", config.IfGetInt("PAGINATION_DEFAULT_LIMIT", 20))
		if e != nil {
			ErrJSON(w, NewBadRequestError(fmt.Sprintf("invalid value for query parameter 'limit': %s", e)))
			return
		}
		if !(limit >= 1) {
			ErrJSON(w, NewBadRequestError(fmt.Sprintf("invalid value for query parameter 'limit': %d", limit)))
			return
		}
		pageMaxLimit := config.IfGetInt("PAGINATION_MAX_LIMIT", 100)
		if limit > pageMaxLimit {
			limit = pageMaxLimit
		}

		log.Debugf("actorType[id]: %s[%s], action: %s, objectType[id]: %s[%s], from: %d, to: %d, offset: %d, limit: %d", actorType, actorID, action, objectType, objectID, from, to, offset, limit)

		eventsJSON, err := svc.Records(actorType, actorID, action, objectType, objectID, failed, from, to, offset, limit)

		if err != nil {
			ErrJSON(w, err)
			return
		}
		OkJSON(w, eventsJSON)
	}
}
