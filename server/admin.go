package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/_metalogic_/build"
	events "bitbucket.org/_metalogic_/events-svc"
	. "bitbucket.org/_metalogic_/glib/http" // dot import fo avoid package prefix in reference (shutup lint)
	_ "bitbucket.org/_metalogic_/glib/types"
	"bitbucket.org/_metalogic_/log"
)

// ServiceInfo gets events-service info
// @Summary get events service info
// @Description get events service info, including version, log level
// @ID get-info
// @Produce json
// @Success 200 {object} build.Runtime
// @Failure 400 {object} ErrorResponse
// @Failure 400 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /info [get]
func APIInfo(svc events.Journal) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {

		rt := &build.Runtime{
			BuildInfo:   build.Info,
			ServiceInfo: svc.Info(),
			LogLevel:    log.GetLevel().String(),
		}

		runtimeJSON, err := json.Marshal(rt)
		if err != nil {
			ErrJSON(w, NewServerError(err.Error()))
			return
		}
		OkJSON(w, string(runtimeJSON))
	}
}

// Health returns 200 ok if events listener and adapter are each healthy
// @Summary check health of events service
// @Description checks health of events service
// @ID get-health
// @Produce  plain
// @Success 200 {string} string "ok"
// @Failure 400 {object} ErrorResponse
// @Failure 404 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /events-api/v1/health [get]
func Health(svc events.Common) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		w.Header().Set("Content-Type", "text/plain")
		err := svc.Health()
		if err != nil {
			http.Error(w, http.StatusText(http.StatusServiceUnavailable)+fmt.Sprintf(": %s", err), http.StatusServiceUnavailable)
			return
		}
		fmt.Fprint(w, "ok\n")
	}
}

// Stats returns API statistics (currently only DB stats)
// @Summary get events service statistics
// @Description get events service statistics
// @ID get-stats
// @Produce  json
// @Success 200 {object} events.Stats
// @Failure 400 {object} ErrorResponse
// @Failure 404 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /events-api/v1/stats [get]
func Stats(svc events.Common) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		w.Header().Set("Content-Type", "application/json")
		stats := svc.Stats()
		fmt.Fprint(w, stats)
	}
}

// LogLevel returns the current log level
// @Summary gets the current service log level
// @Description gets the service log level (one of Trace, Debug, Info, Warn or Error)
// @ID get-get-loglevel
// @Produce json
// @Success 200 {object} events.Message
// @Failure 400 {object} ErrorResponse
// @Failure 404 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /events-api/v1/admin/loglevel [get]
func LogLevel(svc events.Common) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		MsgJSON(w, log.GetLevel().String())
	}
}

// SetLogLevel sets the service log level
// @Summary sets the service log level
// @Description dynamically sets the service log level to one of Trace, Debug, Info, Warn or Error
// @ID get-set-loglevel
// @Accept json
// @Produce json
// @Param verbosity path string true "Log Level"
// @Success 200 {object} events.Message
// @Failure 400 {object} ErrorResponse
// @Failure 404 {object} ErrorResponse
// @Failure 500 {object} ErrorResponse
// @Router /events-api/v1/admin/loglevel [put]
func SetLogLevel(svc events.Common) func(w http.ResponseWriter, r *http.Request, params map[string]string) {
	return func(w http.ResponseWriter, r *http.Request, params map[string]string) {
		var (
			verbosity string
		)

		verbosity = params["verbosity"]
		verbosity = strings.ToLower(verbosity)

		var v log.Level
		switch verbosity {
		case "fatal":
			v = log.FatalLevel
		case "error":
			v = log.ErrorLevel
		case "info":
			v = log.InfoLevel
		case "debug":
			v = log.DebugLevel
		case "trace":
			v = log.TraceLevel
		default:
			ErrJSON(w, NewBadRequestError(fmt.Sprintf("invalid log level: %s", verbosity)))
			return
		}

		log.SetLevel(v)
		MsgJSON(w, v.String())
	}
}
