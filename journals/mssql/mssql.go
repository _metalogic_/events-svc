package mssql

import (
	"context"
	"database/sql"
	"fmt"
	"net/url"
	"strings"
	"time"

	"bitbucket.org/_metalogic_/config"
	events "bitbucket.org/_metalogic_/events-svc"
	. "bitbucket.org/_metalogic_/glib/http" // dot import fo avoid package prefix in reference (shutup lint)
	. "bitbucket.org/_metalogic_/glib/sql"
	"bitbucket.org/_metalogic_/log"

	_ "github.com/denisenkom/go-mssqldb"
)

// MSSql implements the event journal interface
type MSSql struct {
	id      string
	conn    *sql.DB
	context context.Context
	info    map[string]string
	init    func() error
}

func init() {
	events.RegisterJournal(newMSSql("MSSql"))
}

func newMSSql(id string) (mssql *MSSql) {
	// initialize global reference
	mssql = &MSSql{id: id}
	mssql.context = context.TODO()

	// set the init function to be called by svc.Init()
	mssql.init = func() (err error) {
		var (
			user     = config.MustGetConfig("API_DB_USER")
			password = config.MustGetConfig("API_DB_PASSWORD")
			name     = config.MustGetConfig("DB_NAME")
			server   = config.MustGetConfig("DB_HOST")
			port     = config.IfGetInt("DB_PORT", 1433)
		)
		log.Debug("init MSSql journal")
		// configure MSSql Server
		values := url.Values{}
		values.Set("database", name)
		values.Set("app", "events-svc")
		u := &url.URL{
			Scheme: "sqlserver",
			User:   url.UserPassword(user, password),
			Host:   fmt.Sprintf("%s:%d", server, port),
			// Path:  instance, // if connecting to an instance instead of a port
			RawQuery: values.Encode(),
		}

		log.Debugf("%s connection string: %s", "sqlserver", u.Redacted())

		mssql.conn, err = sql.Open("sqlserver", u.String())
		if err != nil {
			log.Fatal(err)
		}

		mssql.info = make(map[string]string)
		mssql.info["Type"] = "sqlserver"
		mssql.info["Version"], err = mssql.getVersion()
		if err != nil {
			log.Error(err.Error())
		}
		mssql.info["Database"] = name

		mssql.conn.SetConnMaxLifetime(300 * time.Second)
		mssql.conn.SetMaxIdleConns(50)
		mssql.conn.SetMaxOpenConns(50)

		return nil
	}

	return mssql
}

func (svc *MSSql) ID() string {
	return svc.id
}

// Init initializes the journal by calling its init function
func (svc *MSSql) Init() error {
	return svc.init()
}

// Common interface methods
func (svc *MSSql) Close() error {
	return svc.conn.Close()
}

func (svc *MSSql) Health() error {
	return svc.conn.Ping()
}

func (svc *MSSql) Info() map[string]string {
	return svc.info
}

func (svc *MSSql) Stats() string {
	dbstats := svc.conn.Stats()
	databaseJSON := fmt.Sprintf("{\"MaxOpenConnections\": %d, \"OpenConnections\" : %d, \"InUse\": %d, \"Idle\": %d, \"WaitCount\": %d, \"WaitDuration\": %d, \"MaxIdleClosed\": %d, \"MaxLifetimeClosed\": %d}",
		dbstats.MaxOpenConnections,
		dbstats.OpenConnections,
		dbstats.InUse,
		dbstats.Idle,
		dbstats.WaitCount,
		dbstats.WaitDuration,
		dbstats.MaxIdleClosed,
		dbstats.MaxLifetimeClosed)
	natsJSON := "TODO"
	return fmt.Sprintf("{\"database\": %s, \"nats\": %s}", databaseJSON, natsJSON)
}

func (svc *MSSql) Records(actorType, actorID, action, objectType, objectID string, failed *bool, from, to int64, offset, limit int) (eventsJSON string, err error) {
	var (
		rowcount int64
		total    int64
		rows     *sql.Rows
	)

	rows, err = svc.conn.QueryContext(svc.context, "[events].[GetRecords]",
		sql.Named("actorType", IfNullString(actorType)),
		sql.Named("actorID", IfNullString(actorID)),
		sql.Named("action", IfNullString(action)),
		sql.Named("objectType", IfNullString(objectType)),
		sql.Named("objectID", IfNullString(objectID)),
		sql.Named("failed", IfNullBoolRef(failed)),
		sql.Named("from", IfNullInt(from, 0)),
		sql.Named("to", IfNullInt(from, 0)),
		sql.Named("offset", offset),
		sql.Named("limit", limit),
		sql.Named("RowCount", sql.Out{Dest: &rowcount}),
		sql.Named("Total", sql.Out{Dest: &total}))

	if err != nil {
		return eventsJSON, NewDBError(err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&eventsJSON)
	}
	if err != nil {
		return eventsJSON, NewDBError(err.Error())
	}

	next := int64(offset) + rowcount
	eventsJSON = fmt.Sprintf("{\"total\" : %d, \"offset\" : %d, \"count\" : %d, \"next\" : %d, \"data\" : %s }", total, offset, rowcount, next, eventsJSON)

	return eventsJSON, nil
}

func (svc *MSSql) Record(msg events.Message) error {
	log.Debugf("recording message: %+v\n", msg)

	result, err := svc.conn.ExecContext(svc.context, "[events].[CreateRecord]",
		sql.Named("Timestamp", msg.Timestamp),
		sql.Named("Targets", IfNullString(msg.Targets)),
		sql.Named("ActorType", strings.ToUpper(msg.ActorType)),
		sql.Named("ActorID", msg.ActorID),
		sql.Named("Action", strings.ToUpper(msg.Action)),
		sql.Named("ObjectType", strings.ToUpper(msg.ObjectType)),
		sql.Named("ObjectID", msg.ObjectID),
		sql.Named("Error", IfNullString(msg.Error)),
	)
	if err != nil {
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rows != 1 {
		return fmt.Errorf("expected single row affected, got %d rows affected", rows)
	}
	return err
}

func (svc *MSSql) getVersion() (version string, err error) {
	_, err = svc.conn.QueryContext(svc.context, "[dbo].[Version]", sql.Named("Version", sql.Out{Dest: &(version)}))
	if err != nil {
		log.Errorf("%s", err)
		return version, err
	}
	return version, nil
}
