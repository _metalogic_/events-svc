package mock

import (
	"encoding/json"

	events "bitbucket.org/_metalogic_/events-svc"
	"bitbucket.org/_metalogic_/log"
)

// Mock implements the event adapter interface
type Mock struct {
	id      string
	journal []events.Message
	init    func() error
}

func init() {
	events.RegisterJournal(newMock("Mock"))
}

func newMock(id string) *Mock {
	m := &Mock{id: id, journal: make([]events.Message, 0)}
	// set the init function
	m.init = func() (err error) {
		log.Debug("init Mock journal")
		return nil
	}

	return m
}

func (m *Mock) ID() string {
	return m.id
}

func (m *Mock) Health() error {
	return nil
}

func (m *Mock) Info() (info map[string]string) {
	info = make(map[string]string)
	return info
}

func (m *Mock) Records(actorType, actorID, action, objectType, objectID string, failed *bool, from, to int64, offset, limit int) (eventsJSON string, err error) {
	data, err := json.Marshal(m.journal)
	if err != nil {
		return eventsJSON, err
	}
	eventsJSON = string(data)
	return eventsJSON, nil
}

func (m *Mock) Record(msg events.Message) error {
	log.Infof("handling message: %+v\n", msg)
	m.journal = append(m.journal, msg)
	return nil
}

// Init initializes the mock journal
func (m *Mock) Init() error {
	return m.init()
}

func (m *Mock) Close() error {
	return nil
}

func (m *Mock) Stats() string {
	return "TODO"
}
