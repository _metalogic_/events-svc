package events

import (
	"encoding/json"
	"fmt"
	"sort"
	"strings"
	"sync"
	"time"
)

var (
	listeners  = make(map[string]Listener)
	listenerMu sync.RWMutex
)

// Message captures the details used to send logging events through a Listener;
type Message struct {
	Timestamp  int64  `json:"timestamp"`
	Targets    string `json:"targets,omitempty"`
	ActorType  string `json:"actorType"`
	ActorID    string `json:"actorID"`
	Action     string `json:"action"`
	ObjectType string `json:"objectType"`
	ObjectID   string `json:"objectID"`
	Error      string `json:"error"`
}

// MetricsMessage captures the details used to send metrics through a Listener;
type MetricsMessage struct {
	Action  string `json:"service"`
	Message string `json:"message"`
}

// Listener is a type that is used as an Event listener.
// Currently only a NATS listener has been implemented; others may be
// added in the future.
type Listener interface {
	// ID returns the Adapter ID
	ID() string
	// Init calls the initialization function for the Listener.
	Init() error
	Close()
	Listen(Journal) error
	Stats() string
}

// RegisterListener registers an Event Listener as a side-effect of importing
// the listener package, eg import .../listeners/nats registers the NATS listener
// This function panics if the listener is already registered.
func RegisterListener(listener Listener) {
	listenerMu.Lock()
	defer listenerMu.Unlock()

	if _, ok := listeners[string(listener.ID())]; ok {
		panic(fmt.Sprintf("adapter already registered: %s", listener.ID()))
	}
	listeners[string(listener.ID())] = listener
}

func GetListener(id string) (listener Listener, err error) {
	if listener, ok := listeners[id]; ok {
		return listener, nil
	}
	return listener, fmt.Errorf("listener %s is not registered", id)
}

// Listeners returns the names of all registered Event Listeners
// in ascending lexicographical order.
func Listeners() []string {
	listenerMu.RLock()
	defer listenerMu.RUnlock()

	names := make([]string, 0, len(listeners))
	for name := range listeners {
		names = append(names, name)
	}

	sort.Strings(names)

	return names
}

// InitListener returns an initialized Event Listener given its registered ID
func InitListener(name string) (listener Listener, err error) {
	listenerMu.RLock()
	defer listenerMu.RUnlock()
	listener, ok := listeners[name]
	if !ok {
		return listener, fmt.Errorf("listener not registered: %s", name)
	}
	err = listener.Init()
	if err != nil {
		return listener, err
	}
	return listener, nil
}

func DecodeMessage(subj string, data []byte) (msg Message, err error) {
	err = json.Unmarshal(data, &msg)
	if err != nil {
		return msg, err
	}
	if msg.Timestamp == 0 {
		msg.Timestamp = time.Now().UnixNano()
	}

	// subject is of the form "events.ActorType" - implementations of Listener
	// are expected to ignore messages that arrive on subjects that don't match
	parts := strings.SplitAfter(subj, ".")
	msg.ActorType = parts[1]
	return msg, nil
}
