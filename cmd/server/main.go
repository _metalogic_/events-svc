package main

// @title events-svc
// @version 1.0
// @description Events Delivery Service
// @termsOfService https://educationplannerbc.ca/terms/

// @contact.name API Developer Support
// @contact.url https://educationplannerbc.ca/support
// @contact.email support@educationplannerbc.ca

// @Schemes http https
// @BasePath /events-api/v1
// @query.collection.format multi

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"bitbucket.org/_metalogic_/build"
	"bitbucket.org/_metalogic_/config"
	"bitbucket.org/_metalogic_/events-svc"
	"bitbucket.org/_metalogic_/events-svc/docs"
	"bitbucket.org/_metalogic_/events-svc/server"
	"bitbucket.org/_metalogic_/log"

	_ "bitbucket.org/_metalogic_/events-svc/journals/mock"
	_ "bitbucket.org/_metalogic_/events-svc/journals/mssql"
	_ "bitbucket.org/_metalogic_/events-svc/listeners/nats"
)

var (
	info        build.BuildInfo
	traceHeader string
	levelFlg    log.Level
	listenerFlg string
	journalFlg  string
	portFlg     string
)

func init() {

	flag.Var(&levelFlg, "level", "set level of logging by the API")
	flag.StringVar(&journalFlg, "j", "MSSql", "journal")
	flag.StringVar(&listenerFlg, "l", "NATS", "listener")
	flag.StringVar(&portFlg, "p", ":8080", "HTTP management listen port")

	var err error
	info = build.Info

	version := info.String()
	command := info.Name()

	flag.Usage = func() {
		fmt.Printf("Project %s:\n\n", version)
		fmt.Printf("Usage: %s -help (this message) | %s [options]:\n\n", command, command)
		flag.PrintDefaults()
	}

	docs.SwaggerInfo.Host = config.MustGetenv("APIS_HOST")
	projTemplate := config.MustGetConfig("OPENAPI_BUILD_TEMPLATE")
	version, err = info.Format(projTemplate)
	if err != nil {
		log.Warning("Failed to format openapi version from template %s: %s", projTemplate, err)
	} else {
		docs.SwaggerInfo.Description = fmt.Sprintf("%s%s", version, docs.SwaggerInfo.Description)
	}
}

func main() {
	flag.Parse()

	traceHeader = config.IfGetenv("TRACE_HEADER_NAME", "X-Trace-Header")

	if levelFlg != log.None {
		log.SetLevel(levelFlg)
	} else {
		loglevel := os.Getenv("LOG_LEVEL")
		if loglevel == "DEBUG" {
			log.SetLevel(log.DebugLevel)
		}
	}

	// get event listener and adapter
	l, err := events.GetListener(listenerFlg)
	if err != nil {
		log.Fatal(err)
	}

	j, err := events.GetJournal(journalFlg)
	if err != nil {
		log.Fatal(err)
	}

	exitDone := &sync.WaitGroup{}
	exitDone.Add(2)

	evtSrv := server.StartEventsServer(portFlg, traceHeader, l, j, exitDone)

	log.Infof("events-svc started with %s listener and %s journal", l.ID(), j.ID())

	// Wait for a SIGINT (perhaps triggered by user with CTRL-C) or SIGTERM (from Docker)
	// Run cleanup when signal is received
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)
	<-stop

	log.Warning("events-svc received stop signal - shutting down services")

	// now close the servers gracefully ("shutdown")
	var ctx context.Context
	var cancel context.CancelFunc

	ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	log.Warning("stopping Events server")
	evtSrv.Shutdown(ctx)

	// wait for goroutines started in StartEventServer() to stop
	exitDone.Wait()

	log.Warning("events server shutdown complete - exiting")
}
