#!/bin/bash

port=":9080"

export NATS_SERVERS="nats://localhost:4222"

echo starting events-svc on http://localhost${port}
echo '---'

./server -p $port -j Mock

