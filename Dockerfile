FROM metalogic/golang:1.18 as builder

ENV GOPRIVATE "bitbucket.org/_metalogic_/*"

COPY ./ /build

WORKDIR /build/cmd/server

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o events-svc .

FROM metalogic/alpine:latest

RUN adduser -u 25010 -g 'Metalogic Application Owner' -D metalogic

WORKDIR /home/metalogic

COPY --from=builder /build/cmd/server/events-svc .
COPY --from=builder /build/docs docs 

USER metalogic

CMD ["./events-svc"]

HEALTHCHECK --interval=30s CMD /usr/local/bin/health http://localhost:8080/${EVENTS_API_BASE}/health

