package events

import (
	"fmt"
	"log"
	"sort"
	"sync"
)

var (
	journals   = make(map[string]Journal)
	journalsMu sync.RWMutex
)

type Record struct {
	Timestamp  int64  `json:"timestamp"`
	ActorType  string `json:"actorType"`
	ActorID    string `json:"actorID"`
	Action     string `json:"action"`
	ObjectType string `json:"objectType"`
	ObjectID   string `json:"objectID"`
	Error      string `json:"error"`
}

type RecordsResponse struct {
	Total  int      `json:"total"`
	Offset int      `json:"offset"`
	Count  int      `json:"count"`
	Next   int      `json:"next"`
	Data   []Record `json:"data"`
}

// Common defines the common service interface
type Common interface {
	Health() error
	Info() map[string]string
	Stats() string
}

// Journal is a type that is used as an event journal.
type Journal interface {
	Common
	// ID returns the Journal ID
	ID() string
	Records(actorType, actorID, action, objectType, objectID string, failed *bool, from, to int64, offset, limit int) (eventsJSON string, err error)
	Record(msg Message) error
	// Init calls the initialization function for the journal
	Init() error
	Close() error
}

// RegisterJournal registers an implementation of journal
// it should be called in the init function as a side-effect of importing
// the journal package, eg import .../journals/mock registers the mock journal
// This function panics if the journal is already registered.
func RegisterJournal(journal Journal) {
	journalsMu.Lock()
	defer journalsMu.Unlock()

	if _, ok := journals[string(journal.ID())]; ok {
		log.Fatalf("journal already registered: %s", journal.ID())
	}
	journals[string(journal.ID())] = journal
}

// GetJournal gets a journal by ID
func GetJournal(id string) (journal Journal, err error) {
	journalsMu.RLock()
	defer journalsMu.RUnlock()
	var ok bool
	if journal, ok = journals[id]; ok {
		if !ok {
			return journal, fmt.Errorf("journal not registered: %s", id)
		}
		err = journal.Init()
		if err != nil {
			return journal, err
		}
		return journal, nil
	}
	return journal, fmt.Errorf("journal %s is not registered", id)
}

// Journals returns the names of all registered journals in ascending lexicographical order.
func Journals() []string {
	journalsMu.RLock()
	defer journalsMu.RUnlock()

	names := make([]string, 0, len(journals))
	for name := range journals {
		names = append(names, name)
	}

	sort.Strings(names)

	return names
}
