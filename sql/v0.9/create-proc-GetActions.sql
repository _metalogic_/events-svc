DROP PROCEDURE IF EXISTS [auth].GetStatuses
GO

CREATE PROCEDURE [auth].GetStatuses
WITH EXEC AS CALLER
AS
BEGIN
  SELECT(
    SELECT Code AS "code",
      Description AS "description",
      Created AS "created",
      CreateUser AS "createUser",
      Updated AS "updated",
      UpdateUser AS "updateUser"
    FROM [auth].[STATUSES]		
    FOR JSON PATH
	) AS statuses
END
GO
