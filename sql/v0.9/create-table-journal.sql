-- Drop JOURNAL Table

IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'[events].[JOURNAL]') AND OBJECTPROPERTY(id, N'IsTable') = 1 )
BEGIN
    DROP TABLE [events].[JOURNAL]
END
GO

-- Create JOURNAL Table

CREATE TABLE
    [events].[JOURNAL](
        [ID] [int] IDENTITY(1,1) NOT NULL,
        [Timestamp] [bigint] NOT NULL,
        [ActorType] VARCHAR(20) NOT NULL,
        [ActorGUID] VARCHAR(36) NOT NULL,
        [Action] VARCHAR(20) NOT NULL,
        [ObjectType] VARCHAR(20) NOT NULL,
        [ObjectGUID] VARCHAR(36) NOT NULL,
        [Error] VARCHAR(1024) NULL,
        CONSTRAINT PK_JOURNAL PRIMARY KEY (ID))
GO

