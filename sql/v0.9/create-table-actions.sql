-- Drop ACTIONS Table

IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'[events].[ACTIONS]') AND OBJECTPROPERTY(id, N'IsTable') = 1 )
BEGIN
    DROP TABLE [events].[ACTIONS]
END
GO

-- Create ACTIONS Table

CREATE TABLE
    [events].[ACTIONS](
        [ID] [int] IDENTITY(1,1) NOT NULL,
        [ActorType] VARCHAR(20) NOT NULL,
        [Code] VARCHAR(20) NOT NULL,
        [Description] VARCHAR(256) NOT NULL,
        [Created] DATETIME NOT NULL CONSTRAINT [DF_ACTIONS_Created] DEFAULT GETDATE(),
        [CreateUser] VARCHAR(36) NOT NULL CONSTRAINT [DF_ACTIONS_CreateUser] DEFAULT 'ROOT',
        [Updated] DATETIME NOT NULL CONSTRAINT [DF_ACTIONS_Updated] DEFAULT GETDATE(),
        [UpdateUser] VARCHAR(36) NOT NULL CONSTRAINT [DF_ACTIONS_UpdateUser] DEFAULT 'ROOT',
        CONSTRAINT PK_ACTIONS PRIMARY KEY (ID),
        CONSTRAINT UK_ACTIONS_ActorType_Code UNIQUE (ActorType,Code))
GO

SET IDENTITY_INSERT [events].ACTIONS ON
GO

INSERT INTO events.ACTIONS (ActorType, Code, Description) VALUES ('USER', 'LOGIN', 'user account login');
INSERT INTO events.ACTIONS (ActorType, Code, Description) VALUES ('USER', 'CONFIRM', 'user account confirm email');
INSERT INTO events.ACTIONS (ActorType, Code, Description) VALUES ('USER', 'LOGOUT', 'user account logout');
INSERT INTO events.ACTIONS (ActorType, Code, Description) VALUES ('USER', 'SET-PASSWORD', 'user account password set (by self or admin)');
INSERT INTO events.ACTIONS (ActorType, Code, Description) VALUES ('USER', 'CREATE', 'user-initiated create object');
INSERT INTO events.ACTIONS (ActorType, Code, Description) VALUES ('USER', 'READ', 'user-initiated read object');
INSERT INTO events.ACTIONS (ActorType, Code, Description) VALUES ('USER', 'UPDATE', 'user-initiated update object');
INSERT INTO events.ACTIONS (ActorType, Code, Description) VALUES ('USER', 'DELETE', 'user-initiated delete object');
INSERT INTO events.ACTIONS (ActorType, Code, Description) VALUES ('INST', 'CREATE', 'institution-initiated create object');
INSERT INTO events.ACTIONS (ActorType, Code, Description) VALUES ('INST', 'READ', 'institution-initiated read object');
INSERT INTO events.ACTIONS (ActorType, Code, Description) VALUES ('INST', 'UPDATE', 'institution-initiated update object');
INSERT INTO events.ACTIONS (ActorType, Code, Description) VALUES ('INST', 'DELETE', 'institution-initiated delete object');
GO

SET IDENTITY_INSERT [events].ACTIONS OFF
GO
