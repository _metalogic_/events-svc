-- Drop OBJECT_TYPES Table

IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'[events].[OBJECT_TYPES]') AND OBJECTPROPERTY(id, N'IsTable') = 1 )
BEGIN
    DROP TABLE [events].[OBJECT_TYPES]
END
GO

-- Create OBJECT_TYPES Table

CREATE TABLE
    [events].[OBJECT_TYPES](
        [ID] [int] IDENTITY(1,1) NOT NULL,
        [Code] VARCHAR(20) NOT NULL,
        [Description] VARCHAR(256) NOT NULL,
        [Created] DATETIME NOT NULL CONSTRAINT [DF_OBJECT_TYPES_Created] DEFAULT GETDATE(),
        [CreateUser] VARCHAR(36) NOT NULL CONSTRAINT [DF_OBJECT_TYPES_CreateUser] DEFAULT 'ROOT',
        [Updated] DATETIME NOT NULL CONSTRAINT [DF_OBJECT_TYPES_Updated] DEFAULT GETDATE(),
        [UpdateUser] VARCHAR(36) NOT NULL CONSTRAINT [DF_OBJECT_TYPES_UpdateUser] DEFAULT 'ROOT',
        CONSTRAINT PK_OBJECT_TYPES PRIMARY KEY (ID),
        CONSTRAINT UK_OBJECT_TYPES_Code UNIQUE (Code))
GO

SET IDENTITY_INSERT [events].OBJECT_TYPES ON
GO

INSERT INTO events.OBJECT_TYPES (ID, Code, Description) VALUES (1, 'FILE', 'file object');
INSERT INTO events.OBJECT_TYPES (ID, Code, Description) VALUES (2, 'INST', 'institution object');
INSERT INTO events.OBJECT_TYPES (ID, Code, Description) VALUES (3, 'SESSION', 'application session object');
INSERT INTO events.OBJECT_TYPES (ID, Code, Description) VALUES (4, 'USER', 'user account object');
INSERT INTO events.OBJECT_TYPES (ID, Code, Description) VALUES (5, 'USER-INFO', 'user personal info object (name, address, citizenship, phone, email, etc)');
GO

SET IDENTITY_INSERT [events].OBJECT_TYPES OFF
GO

