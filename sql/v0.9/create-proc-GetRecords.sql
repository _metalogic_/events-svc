CREATE PROCEDURE "events"."GetRecords"
@ActorType VARCHAR(20),
@ActorID VARCHAR(36), 
@Action VARCHAR(20),
@ObjectType VARCHAR(20),
@ObjectID VARCHAR(36),
@Failed BIT,
@To BIGINT,
@From BIGINT,
@Offset INT,
@Limit INT,
@RowCount INT OUTPUT,
@Total INT OUTPUT
WITH EXEC AS CALLER
AS
BEGIN
  DECLARE @BaseCode INT = 50000
  DECLARE @ReturnCode INT
  DECLARE @Message VARCHAR(200)

  BEGIN TRY

    IF @ActorType IS NOT NULL AND NOT EXISTS (SELECT * FROM [events].[ACTOR_TYPES] WHERE Code = @ActorType)
    BEGIN
      SET @ReturnCode = @BaseCode + 404;
      SET @Message = 'no actor type found with code: ' + @ActorType;
      THROW @ReturnCode, @Message, 1;
    END
    
    IF @Action IS NOT NULL AND NOT EXISTS (SELECT * FROM [events].[ACTIONS] WHERE Code = @Action)
    BEGIN
      SET @ReturnCode = @BaseCode + 404;
      SET @Message = 'no action found with code: ' + @Action;
      THROW @ReturnCode, @Message, 1;
    END
    
    IF @ObjectType IS NOT NULL AND NOT EXISTS (SELECT * FROM [events].[OBJECT_TYPES] WHERE Code = @ObjectType)
    BEGIN
      SET @ReturnCode = @BaseCode + 404;
      SET @Message = 'no object type found with code: ' + @ObjectType;
      THROW @ReturnCode, @Message, 1;
    END
    

    SET @Total = (
        SELECT COUNT(*)
        FROM [events].[JOURNAL] [j]
        WHERE (@ActorType IS NULL OR [j].ActorType = @ActorType)
        AND (@ActorID IS NULL OR [j].ActorGUID = @ActorID)
        AND (@Action IS NULL OR [j].Action = @Action)
        AND (@ObjectType IS NULL OR [j].ObjectType = @ObjectType)
        AND (@ObjectID IS NULL OR [j].ObjectGUID = @ObjectID)
        AND (@Failed IS NULL
             OR (@Failed = 1 AND [j].Error IS NOT NULL)
             OR (@Failed = 0 AND [j].Error IS NULL))
        AND (@To IS NULL OR [j].Timestamp <= @To)
        AND (@From IS NULL OR [j].Timestamp >= @From)
    )

    DECLARE @json NVARCHAR(MAX)
    
    IF @Total > 0
    BEGIN
        SET @json = (
        SELECT [Timestamp] AS "timestamp",
               [ActorType] AS "actorType",
               [ActorGUID] AS "actorID", 
               [Action] AS "action", 
               [ObjectType] AS "objectType", 
               [ObjectGUID] AS "objectID", 
               [Error] AS "error"
        FROM [events].[JOURNAL] [j]
        WHERE (@ActorType IS NULL OR [j].ActorType = @ActorType)
        AND (@ActorID IS NULL OR [j].ActorGUID = @ActorID)
        AND (@Action IS NULL OR [j].Action = @Action)
        AND (@ObjectType IS NULL OR [j].ObjectType = @ObjectType)
        AND (@ObjectID IS NULL OR [j].ObjectGUID = @ObjectID)
        AND (@Failed IS NULL
             OR (@Failed = 1 AND [j].Error IS NOT NULL)
             OR (@Failed = 0 AND [j].Error IS NULL))
        AND (@To IS NULL OR [j].Timestamp <= @To)
        AND (@From IS NULL OR [j].Timestamp >= @From)
        ORDER BY [j].ID DESC OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY
        FOR JSON PATH, INCLUDE_NULL_VALUES)

        SET @RowCount = (SELECT COUNT(*) FROM OPENJSON(@json))
    END

    SELECT ISNULL(@json, '[]')
    
  END TRY
  
  BEGIN CATCH
    IF ERROR_NUMBER() > 50000
    BEGIN
      THROW;
    END
    DECLARE @ErrorMessage VARCHAR(400)
    SELECT @ErrorMessage = 'Getting journal records failed: ' + ERROR_MESSAGE();
    THROW 50000, @ErrorMessage, 1;
  END CATCH

END

