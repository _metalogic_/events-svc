-- Add foreign key to events.JOURNAL

ALTER TABLE [events].[JOURNAL] ADD CONSTRAINT FK_JOURNAL_ActorType_Action FOREIGN KEY ("ActorType","Action") REFERENCES 
[events].[ACTIONS] ("ActorType","Code")
GO

ALTER TABLE [events].[JOURNAL] ADD CONSTRAINT FK_JOURNAL_ObjectType FOREIGN KEY ("ObjectType") REFERENCES 
[events].[OBJECT_TYPES] ("Code")
GO


