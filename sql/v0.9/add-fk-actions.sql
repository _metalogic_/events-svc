-- Add foreign key to events.ACTIONS

ALTER TABLE [events].[ACTIONS] ADD CONSTRAINT FK_ACTIONS_ActorType FOREIGN KEY ("ActorType") REFERENCES 
[events].[ACTOR_TYPES] ("Code")
GO

