ALTER PROCEDURE [events].[CreateRecord]
  @Timestamp BIGINT,
  @Targets VARCHAR(740),
  @ActorType VARCHAR(20),
  @ActorID VARCHAR(36),
  @Action VARCHAR(20),
  @ObjectType VARCHAR(20),
  @ObjectID VARCHAR(36),
  @Error VARCHAR(1024)
WITH EXEC AS CALLER
AS
BEGIN
  DECLARE @BaseCode INT = 50000
  DECLARE @ReturnCode INT
  DECLARE @Message VARCHAR(200)

  BEGIN TRY

    IF NOT EXISTS (SELECT * FROM [events].[ACTOR_TYPES] WHERE Code = @ActorType)
    BEGIN
      SET @ReturnCode = @BaseCode + 404;
      SET @Message = 'no actor type found with code: ' + @ActorType;
      THROW @ReturnCode, @Message, 1;
    END
    
    IF NOT EXISTS (SELECT * FROM [events].[ACTIONS] WHERE Code = @Action)
    BEGIN
      SET @ReturnCode = @BaseCode + 404;
      SET @Message = 'no action found with code: ' + @Action;
      THROW @ReturnCode, @Message, 1;
    END
    
    IF NOT EXISTS (SELECT * FROM [events].[OBJECT_TYPES] WHERE Code = @ObjectType)
    BEGIN
      SET @ReturnCode = @BaseCode + 404;
      SET @Message = 'no object type found with code: ' + @ObjectType;
      THROW @ReturnCode, @Message, 1;
    END
    
    INSERT INTO [events].[JOURNAL] 
       ([Timestamp], [Targets], [ActorType], [ActorGUID], [Action], [ObjectType], [ObjectGUID], [Error])
    VALUES 
       (@Timestamp, @Targets, @ActorType, @ActorID, @Action, @ObjectType, @ObjectID, @Error)    
     
    DECLARE @journalID INT
    SELECT @journalID = SCOPE_IDENTITY()

    SELECT (SELECT [Timestamp] AS "timestamp",
                   [Targets] AS "targets",
                   [ActorType] AS "actorType",
                   [ActorGUID] AS "actorID", 
                   [Action] AS "action", 
                   [ObjectType] AS "objectType", 
                   [ObjectGUID] AS "objectID", 
                   [Error] AS "error"
    FROM [events].[JOURNAL]
    WHERE ID = @journalID
    FOR JSON PATH, WITHOUT_ARRAY_WRAPPER, INCLUDE_NULL_VALUES) AS journal

  END TRY
  
  BEGIN CATCH
    IF ERROR_NUMBER() > 50000
    BEGIN
      THROW;
    END
    DECLARE @ErrorMessage VARCHAR(400)
    SELECT @ErrorMessage = 'create journal failed: ' + ERROR_MESSAGE();
    THROW 50000, @ErrorMessage, 1;
  END CATCH
END

